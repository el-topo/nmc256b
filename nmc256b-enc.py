#!/usr/bin/python3
# Base64 encodes a file and spits out a JSON string for possible inclusion
# in the Namecoin blockchain.
# Copyright (C) 2018 El Topo

# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.

# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.

# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.

import sys
import base64

if len(sys.argv) < 2:
    sys.exit('Usage: %s [FILE]' % sys.argv[0])

with open(sys.argv[1], mode='rb') as infile:
    mybytes = infile.read()

mybase64 = base64.b64encode(mybytes)
mybase64 = mybase64.decode()
print("{\"name\" : \"", sys.argv[1], "\", \"base64\" : \"", mybase64, "\"}", sep="")
