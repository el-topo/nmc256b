# nmc256b

Python scripts for encoding and extracting tiny executables out of the
Namecoin blockchain. It has been tested with demoscene productions
(look it up) of 256 bytes in size but the Namecoin id/-fields allows
for 520 bytes of data. Remember that the base64 encoding adds some
overhead.

Tested with GNU/Linux (Trisquel 7.0).

Requirements:

* Python 3
* A running (and fully synced) Namecoin blockchain.

## Usage

`nmc256b-enc.py [FILE]`

Will base64 encode `[FILE]` and return a JSON string to stdout that
you can insert as an id/ into the Namecoin blockchain.

`nmc256b-pop.py [256B PROD ID]` i.e. `id/57280`

Will extract a production from the blockchain and write it to disk as
a executable binary.

This project is a quick hack and the number of useful error messages
from the scripts are limited.

I have chosen to name the id's after their corresponding pouet.net
id's, this is not a requirement though.

## Currently encoded prods

These are all for the Amiga OCS/ECS platform.

* [Hotbleeps by Loonies](https://www.pouet.net/prod.php?which=57065) id/57065
* [Circus by Depth](https://www.pouet.net/prod.php?which=57280) id/57280
* [Authentic World of Commodore colors by Loonies](https://www.pouet.net/prod.php?which=57710) id/57710
* [Nullbyte by Piru](https://www.pouet.net/prod.php?which=63326) id/63326
* [Benoît by Orb](https://www.pouet.net/prod.php?which=65455) id/65455
* [Glitch My Bits Up by Wanted Team](https://www.pouet.net/prod.php?which=78515) id/78515

-- El Topo
