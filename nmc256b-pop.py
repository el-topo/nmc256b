#!/usr/bin/python3
# Proof-of-concept retrieval of 256b prods from the Namecoin blockchain.
# Copyright (C) 2018 El Topo

# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.

# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.

# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.

import base64
import json
import subprocess
import sys

if len(sys.argv) < 2:
    sys.exit('Usage: %s [256B PROD ID]' % sys.argv[0])

cmdstring = ' '.join(['namecoin-cli name_show', sys.argv[1]])
try:
    output = subprocess.check_output(cmdstring, shell=True)
    output = output.decode('utf-8')
except:
    sys.exit("Catastrophic failure, exiting.")

jstring = json.loads(output)['value']
filename = json.loads(jstring)['name']
mybase64 = json.loads(jstring)['base64']

mybase64 = str.encode(mybase64)
mybin = base64.decodebytes(mybase64)

myfile = open(filename, 'wb')
print("Writing file \"", filename, "\".", sep='')
myfile.write(mybin)
myfile.close()
